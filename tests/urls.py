# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include

from common.urls import urlpatterns as common_urls

urlpatterns = [
    url(r'^', include(common_urls, namespace='common')),
]
