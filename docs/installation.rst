============
Installation
============

At the command line::

    $ easy_install django-common-models

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-common-models
    $ pip install django-common-models
