=====
Usage
=====

To use django-common-models in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'common.apps.CommonConfig',
        ...
    )

Add django-common-models's URL patterns:

.. code-block:: python

    from common import urls as common_urls


    urlpatterns = [
        ...
        url(r'^', include(common_urls)),
        ...
    ]
